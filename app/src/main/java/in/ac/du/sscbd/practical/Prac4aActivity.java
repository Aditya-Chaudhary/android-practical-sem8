package in.ac.du.sscbd.practical;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Prac4aActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText messageEditText;
    private Button sendButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prac4a);

        messageEditText = (EditText) findViewById(R.id.message_3a_text_view);
        sendButton = (Button) findViewById(R.id.send_button_3a);
        sendButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.send_button_3a:
                String msg = messageEditText.getText().toString();
                if (!msg.isEmpty()) {
                    Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_SEND);
                    intent.putExtra(Intent.EXTRA_TEXT, msg);
                    intent.setType("text/plain");
                    Intent chooser = Intent.createChooser(intent, "Choose an App to SEND MESSAGE");
                    if (intent.resolveActivity(getPackageManager()) != null) {
                        startActivity(chooser);
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Enter message first", Toast.LENGTH_SHORT).show();
                }
                break;
        }

    }
}

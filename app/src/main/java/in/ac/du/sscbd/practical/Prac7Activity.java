package in.ac.du.sscbd.practical;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.RadioGroup;
import android.widget.TextView;

public class Prac7Activity extends AppCompatActivity implements RadioGroup.OnCheckedChangeListener{

    private RadioGroup radioGroup;
    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prac7);

        radioGroup = (RadioGroup) findViewById(R.id.radio_group);
        textView = (TextView) findViewById(R.id.teacher_incharge_text_view);
        radioGroup.setOnCheckedChangeListener(this);
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId){
            case R.id.radio_btech:
                textView.setText("Teacher In-Charge: Mr. Ajay Jaiswal");
                break;
            case R.id.radio_bms:
                textView.setText("Teacher In-Charge: Mrs. Gurpreet");
                break;
            case R.id.radio_bsc:
                textView.setText("Teacher In-Charge: Mr. Sammer Anand");
                break;
        }
    }
}

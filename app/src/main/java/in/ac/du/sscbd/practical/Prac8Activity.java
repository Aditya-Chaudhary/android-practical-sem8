package in.ac.du.sscbd.practical;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Prac8Activity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prac8);

        Button red = (Button) findViewById(R.id.red_button);
        Button green = (Button) findViewById(R.id.green_button);
        Button blue = (Button) findViewById(R.id.blue_button);

        red.setOnClickListener(this);
        green.setOnClickListener(this);
        blue.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        View root = v.getRootView();

        switch (v.getId()){
            case R.id.red_button:
                root.setBackgroundColor(getResources().getColor(android.R.color.holo_red_light));
                break;
            case R.id.green_button:
                root.setBackgroundColor(getResources().getColor(android.R.color.holo_green_light));
                break;
            case R.id.blue_button:
                root.setBackgroundColor(getResources().getColor(android.R.color.holo_blue_light));
                break;
        }
    }
}

package in.ac.du.sscbd.practical;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Prac11bActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView textView;
    private Button logoutButton;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prac11b);

        context = this;

        textView = (TextView) findViewById(R.id.username_text_view);
        textView.setText("Hi, " + getIntent().getStringExtra("uname"));

        logoutButton = (Button) findViewById(R.id.logout_button);
        logoutButton.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.logout_button:

                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        finish();
                    }
                });
                builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                        dialog.dismiss();
                    }
                });

                builder.setMessage("Are you sure you want to logout?")
                        .setTitle("Logout");
                AlertDialog dialog = builder.create();
                dialog.show();

                break;
        }
    }
}

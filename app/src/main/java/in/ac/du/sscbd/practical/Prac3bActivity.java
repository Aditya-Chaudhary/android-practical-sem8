package in.ac.du.sscbd.practical;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class Prac3bActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prac3b);

        TextView textView = (TextView) findViewById(R.id.message_3b_text_view);
        String msg = getIntent().getStringExtra("message");
        textView.setText(msg);


    }
}

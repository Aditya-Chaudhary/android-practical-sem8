package in.ac.du.sscbd.practical;

import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import in.ac.du.sscbd.practical.helpers.NotesDBHelper;

public class Prac12Activity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemClickListener {

    SQLiteDatabase db_read, db_write;
    NotesDBHelper notesDBHelper;

    ListView listView;
    EditText editText;
    Button addNotesButton;
    Context context;

    ArrayAdapter<String> adapter;

    List noteIdsList = new ArrayList<>();
    List notesList = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prac12);

        context = this;

        notesDBHelper = new NotesDBHelper(this, "mydb");
        db_read = notesDBHelper.getReadableDatabase();
        db_write = notesDBHelper.getWritableDatabase();

        listView = (ListView) findViewById(R.id.notes_list_view);
        editText = (EditText) findViewById(R.id.notes_edit_text);
        addNotesButton = (Button) findViewById(R.id.add_notes_button);
        addNotesButton.setOnClickListener(this);

        adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, notesList);

        listView.setAdapter(adapter);
        listView.setOnItemClickListener(this);

        readData();

    }


    private void readData() {

        String[] projection = {
                NotesDBHelper.COLUMN_ID,
                NotesDBHelper.COLUMN_NOTE
        };

        String sortOrder = NotesDBHelper.COLUMN_ID + " DESC";

        Cursor cursor = db_read.query(
                NotesDBHelper.TABLE_NAME,
                projection,
                null,
                null,
                null,
                null,
                sortOrder
        );

        noteIdsList.clear();
        notesList.clear();

        while (cursor.moveToNext()) {
            long itemId = cursor.getLong(cursor.getColumnIndexOrThrow(NotesDBHelper.COLUMN_ID));
            String note = cursor.getString(cursor.getColumnIndexOrThrow(NotesDBHelper.COLUMN_NOTE));

            noteIdsList.add(itemId);
            notesList.add(note);
        }

        adapter.notifyDataSetChanged();

        cursor.close();

    }

    private void editNote(long id, String note) {

        ContentValues values = new ContentValues();
        values.put(NotesDBHelper.COLUMN_NOTE, note);

        String selection = NotesDBHelper.COLUMN_ID + " = ?";
        String[] selectionArgs = {String.valueOf(id)};

        int count = db_read.update(
                NotesDBHelper.TABLE_NAME,
                values,
                selection,
                selectionArgs
        );
        readData();
    }

    private void deleteNote(long id) {
        String selection = NotesDBHelper.COLUMN_ID + " = ?";
        String[] selectionArgs = {String.valueOf(id)};
        db_write.delete(NotesDBHelper.TABLE_NAME, selection, selectionArgs);
        readData();
    }

    @Override
    protected void onDestroy() {
        super.onPause();
        db_read.close();
        db_write.close();
        notesDBHelper.close();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.add_notes_button:

                String msg = editText.getText().toString();
                if (msg.isEmpty()) {
                    Toast.makeText(context, "Enter a Note Text", Toast.LENGTH_SHORT).show();
                    return;
                }

                ContentValues values = new ContentValues();
                values.put(NotesDBHelper.COLUMN_NOTE, msg);
                db_write.insert(NotesDBHelper.TABLE_NAME, null, values);

                editText.setText("");
                readData();
                break;
        }

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        final long dbPos = (long) noteIdsList.get(position);
        String itemValue = (String) notesList.get(position);

        /*Toast.makeText(getApplicationContext(),
                "Position :" + position + " DB Pos:" + dbPos + "  ListItem : " + itemValue, Toast.LENGTH_LONG)
                .show();*/


        LayoutInflater li = LayoutInflater.from(context);
        View promptsView = li.inflate(R.layout.dialog_edit_note, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                context);

        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setView(promptsView);

        final EditText userInput = (EditText) promptsView
                .findViewById(R.id.dialog_notes_edit_text);
        Button deleteButton = (Button) promptsView.findViewById(R.id.dialog_delete_button);
        Button updateButton = (Button) promptsView.findViewById(R.id.dialog_update_button);
        Button cancelButton = (Button) promptsView.findViewById(R.id.dialog_cancel_button);


        // set dialog message
        alertDialogBuilder
                .setCancelable(true);

        // create alert dialog
        final AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();

        userInput.setText(itemValue);


        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.dialog_delete_button:
                        deleteNote(dbPos);
                        alertDialog.dismiss();
                        break;
                    case R.id.dialog_update_button:
                        String note = userInput.getText().toString();
                        if (note.isEmpty()) {
                            Toast.makeText(v.getContext(), "Enter a Note Text", Toast.LENGTH_SHORT).show();
                            return;
                        }
                        editNote(dbPos, note);
                        alertDialog.dismiss();

                        break;
                    case R.id.dialog_cancel_button:
                        alertDialog.dismiss();
                        break;
                }
            }
        };

        deleteButton.setOnClickListener(listener);
        updateButton.setOnClickListener(listener);
        cancelButton.setOnClickListener(listener);

    }
}

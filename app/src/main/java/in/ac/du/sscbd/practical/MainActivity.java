package in.ac.du.sscbd.practical;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private ListView practicalListView;

    String practicals[] = new String[]{
            "Practical 1: Hello World",
            "Practical 2: Android Activity Life Cycle Phases",
            "Practical 3: EditText & Send Button",
            "Practical 4: EditText, Send Button & Implicit Intent(SEND ACTION)",
            "Practical 5: Spinner & Image Change",
            "Practical 6: Menu",
            "Practical 7: Radio Button",
            "Practical 8: Vertically aligned buttons and screen color",
            "Practical 9: Horizontally aligned buttons and screen color",
            "Practical 10: Login & Popup message",
            "Practical 11: Login and Login navigation",
            "Practical 12: Database transaction"
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        /*FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, practicals);


        practicalListView = (ListView) findViewById(R.id.practicals_list_view);
        practicalListView.setAdapter(adapter);
        practicalListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // ListView Clicked item index
                int itemPosition = position;

                // ListView Clicked item value
                String itemValue = (String) practicalListView.getItemAtPosition(position);

                Intent intent;
                switch (itemPosition + 1) {
                    case 1:
                        intent = new Intent(getApplicationContext(), Prac1Activity.class);
                        startActivity(intent);
                        break;
                    case 2:
                        intent = new Intent(getApplicationContext(), Prac2Activity.class);
                        startActivity(intent);
                        break;
                    case 3:
                        intent = new Intent(getApplicationContext(), Prac3aActivity.class);
                        startActivity(intent);
                        break;
                    case 4:
                        intent = new Intent(getApplicationContext(), Prac4aActivity.class);
                        startActivity(intent);
                        break;
                    case 5:
                        intent = new Intent(getApplicationContext(), Prac5Activity.class);
                        startActivity(intent);
                        break;
                    case 6:
                        intent = new Intent(getApplicationContext(), Prac6Activity.class);
                        startActivity(intent);
                        break;
                    case 7:
                        intent = new Intent(getApplicationContext(), Prac7Activity.class);
                        startActivity(intent);
                        break;
                    case 8:
                        intent = new Intent(getApplicationContext(), Prac8Activity.class);
                        startActivity(intent);
                        break;
                    case 9:
                        intent = new Intent(getApplicationContext(), Prac9Activity.class);
                        startActivity(intent);
                        break;
                    case 10:
                        intent = new Intent(getApplicationContext(), Prac10Activity.class);
                        startActivity(intent);
                        break;
                    case 11:
                        intent = new Intent(getApplicationContext(), Prac11aActivity.class);
                        startActivity(intent);
                        break;
                    case 12:
                        intent = new Intent(getApplicationContext(), Prac12Activity.class);
                        startActivity(intent);
                        break;
                    default:
                        Toast.makeText(getApplicationContext(),
                                "Position :" + itemPosition + "  ListItem : " + itemValue, Toast.LENGTH_LONG)
                                .show();
                        break;
                }
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        String uname = "";
        try {
            Cursor c = getApplication().getContentResolver().query(ContactsContract.Profile.CONTENT_URI, null, null, null, null);
            c.moveToFirst();
            uname = c.getString(c.getColumnIndex("display_name"));
            c.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            uname = "BTech 4A";
        }

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_author) {
            Toast.makeText(getApplicationContext(), "Prepared By: " + uname, Toast.LENGTH_LONG).show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}

package in.ac.du.sscbd.practical;

import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Objects;

public class Prac10Activity extends AppCompatActivity implements View.OnClickListener {

    private EditText unameEditText;
    private EditText passEditText;
    private Button loginButton;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prac10);

        context = this;

        unameEditText = (EditText) findViewById(R.id.username_edit_text);
        passEditText = (EditText) findViewById(R.id.password_edit_text);
        loginButton = (Button) findViewById(R.id.login_button);
        loginButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.login_button:

                String uname = unameEditText.getText().toString();
                String pass = passEditText.getText().toString();

                if (uname.equals("user") && pass.equals("pass")) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(context);

                    builder.setMessage("Login successful!")
                            .setTitle("Welcome " + uname);

                    AlertDialog dialog = builder.create();
                    dialog.show();

                    unameEditText.setText("");
                    passEditText.setText("");
                } else {
                    Toast.makeText(getApplicationContext(), "Invalid Username or Password", Toast.LENGTH_SHORT).show();
                }


                break;
        }
    }
}

package in.ac.du.sscbd.practical;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Prac3aActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText messageEditText;
    private Button sendButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prac3a);

        messageEditText = (EditText) findViewById(R.id.message_3a_text_view);
        sendButton = (Button) findViewById(R.id.send_button_3a);
        sendButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.send_button_3a:
                String msg = messageEditText.getText().toString();
                if (!msg.isEmpty()) {
                    Intent intent = new Intent(getApplicationContext(), Prac3bActivity.class);
                    intent.putExtra("message", msg);
                    startActivity(intent);
                }else{
                    Toast.makeText(getApplicationContext(), "Enter message first", Toast.LENGTH_SHORT).show();
                }
                break;
        }

    }
}

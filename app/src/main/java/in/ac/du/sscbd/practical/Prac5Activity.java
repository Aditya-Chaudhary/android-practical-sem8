package in.ac.du.sscbd.practical;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;

public class Prac5Activity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    private ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prac5);

        imageView = (ImageView) findViewById(R.id.image_view);

        Spinner spinner = (Spinner) findViewById(R.id.spinner_5);
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.IMAGES_array, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        Bitmap bImage;

        switch (position){
            case 0:
                bImage = BitmapFactory.decodeResource(this.getResources(), R.drawable.saturn);
                imageView.setImageBitmap(bImage);
                break;
            case 1:
                bImage = BitmapFactory.decodeResource(this.getResources(), R.drawable.earth);
                imageView.setImageBitmap(bImage);
                break;
            case 2:
                bImage = BitmapFactory.decodeResource(this.getResources(), R.drawable.mars);
                imageView.setImageBitmap(bImage);
                break;
        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}

package in.ac.du.sscbd.practical;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.util.Date;

public class Prac2Activity extends AppCompatActivity {

    TextView pracLogsTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prac2);
        pracLogsTextView = (TextView) findViewById(R.id.prac2_logs_text_view);

        Toast.makeText(this, "onCreate", Toast.LENGTH_SHORT).show();
        String msg = System.getProperty("line.separator") + "\n[#]"+ DateFormat.getDateTimeInstance().format(new Date()) + " :onCreate";
        pracLogsTextView.append(msg);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Toast.makeText(this, "onStart", Toast.LENGTH_SHORT).show();
        String msg = System.getProperty("line.separator") + "[#]"+ DateFormat.getDateTimeInstance().format(new Date()) + " :onStart";
        pracLogsTextView.append(msg);
    }

    @Override
    protected void onStop() {
        super.onStop();
        Toast.makeText(this, "onStop", Toast.LENGTH_SHORT).show();
        String msg = System.getProperty("line.separator") + "[#]"+ DateFormat.getDateTimeInstance().format(new Date()) + " :onStop";
        pracLogsTextView.append(msg);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Toast.makeText(this, "onDestroy", Toast.LENGTH_SHORT).show();
        String msg = System.getProperty("line.separator") + "[#]"+ DateFormat.getDateTimeInstance().format(new Date()) + " :onDestroy";
        pracLogsTextView.append(msg);
    }

    @Override
    protected void onPause() {
        super.onPause();
        Toast.makeText(this, "onPause", Toast.LENGTH_SHORT).show();
        String msg = System.getProperty("line.separator") + "[#]"+ DateFormat.getDateTimeInstance().format(new Date()) + " :onPause";
        pracLogsTextView.append(msg);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Toast.makeText(this, "onResume", Toast.LENGTH_SHORT).show();
        String msg = System.getProperty("line.separator") + "[#]"+ DateFormat.getDateTimeInstance().format(new Date()) + " :onResume";
        pracLogsTextView.append(msg);

    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Toast.makeText(this, "onRestart", Toast.LENGTH_SHORT).show();
        String msg = System.getProperty("line.separator") + "[#]"+ DateFormat.getDateTimeInstance().format(new Date()) + " :onRestart";
        pracLogsTextView.append(msg);
    }
}
